# Yokai Gameplay

### El GRAN Reinicio

- Hash y Co pasan a ser protagonistas
- Se pasa a formato como el comic de Matrix
- Acomodar esto una vez que esten listos 12 comics como minimo

### Objectivo

Este sera el sitio web de yokai gameplay baserse en el sitio japones de la franquicia, no una copia exacta, pero misma idea

### Secciones

- Pagina principal con la ultima tira y noticias
- Archivo, separado por juego
- Una seccion para los Yokai aliados por juego
- Enlaces varios
- Feed RSS

### Planeador

1. Reacomodar el diseño principal para realinar todo
2. Diseñar el sitio del comic usando este como base, pero con arte original - [Pagina Japonesa de Yokai Watch](https://www.youkai-watch.jp/)
3. Reiniciar el comic con Hash en el papel de Nate y los demas personajes en los demas papeles
4. Rehacer el logo de la pagina japonesa con Hash y Monad
5. La pagina anterior es una imagen del comic mas pequeño y en escala de grises cortesia de css, con algun efecto
6. El diseño de la pagina es tipo feed, con un rectangulo con un sprite/preview del comic, descripcion y el enlace, digamos unos 13 por pagina y luego el next
7. Ver si con JS se pude hacer los botones para avanzar de 13 en 13 tarjetas de comic
8. Usando JS insertar el fooder con el enlace a ko-fi
9. Crear boton de sporte Ko-FI con Whisper sosteniendo el cafe
10. Crear imagenes pares de "comerciales" para cubrir retrazos un par de comerciales da 2 semanas extra
11. Hacer a los Yokai al estilo de la casa mas angulosos.
12. No indentico, pero usar cosas del diseño basico del sitio del webcomic https://www.maiuscentral.com/jrs/comic/the-query-part-3/

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
